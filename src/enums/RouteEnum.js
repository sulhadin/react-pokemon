export const RouteEnum = {
  DEFAULT: '/',
  POKEMON: '/pokemon',
  POKEMON_DETAIL: '/pokemon/:name',
  PAGE_404: '/page-404',
};
