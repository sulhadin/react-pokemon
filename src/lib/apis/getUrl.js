export const getUrl = (path) => {
  return `https://pokeapi.co/api/v2${path}`;
};
