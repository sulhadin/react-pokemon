[![CircleCI](https://circleci.com/gh/sulhadin/react-pokemon.svg?style=svg)](https://circleci.com/gh/sulhadin/react-pokemon)

# React Pokemon

![Pokemon](https://external-preview.redd.it/tQged7mKJ3cUpNMq5IMeceZvyKP3cTyHqhNmKEQ0Vv8.png?auto=webp&s=fb5fd61cae0bc9cde2bc2a006b1e2aeb0c935ce9)

#### What is React Pokemon?
This application includes a list of pokemon along with their pictures as well as their profile pages. 
The application consists of two pages which are list and profile.

This is a sample React app that I want to show my skills about the following areas;

- React knowledge
- Project architecture
- Css skills
- ECMAScript6 skills
- Clean code

This application includes the following libraries;

- **hooks**
- **tests**
- **linter**
- **webpack** configured for both development and production
- **Docker**

The purpose of the project is not to have a complete Pokemon web view app but rather to have
a sandbox repository that I can show my skills on listed libraries.

MIT License
